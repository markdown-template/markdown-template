<!-- Headings -->
# Heading 1
## Heading 2
### Heading 3
#### Heading 4
##### Heading 5

<!-- Italics -->
*This text* is italic
_This text_ is italic

<!-- Strong -->
**This text** is strong
__This text__ is strong

<!-- Strikethrough -->
~~This text~~ is strikethrough

<!-- Horizontal Rule -->
---
___

<!-- Blockquote -->
> this is a quote

<!-- Links -->
[Text for link](http://www.traversymedia.com)
[Text for link](http://www.traversymedia.com "Title")

<!-- Unordered lists -->
* Item 1
* Item 2
    * Nested Item 1
    * Nested Item 2

<!-- Ordered lists -->
1. Item 1
1. Item 2

<!-- Inline code block -->
`<p>This is a paragraph</p>`

<!-- Multi-line code block -->
```
const result = useMemo(() => {
const fib = fibonacci(20);

    console.log("Resut: ", fib);

        return fib;

}, [])
```

<!-- Images -->
![Markdown Logo](https://markdown-here.com/img/icon256.png)


